﻿# Języki obiektowe 2 (Java)

## Uniwersytet WSB Merito w Poznaniu - Studia II stopnia - Kierunek Informatyka

### Informacje wstępne

Kod znajdujący się w repozytorium został przygotowany jako wsparcie dla zajęć realizowanych na Uniwersytecie WSB Merito w Poznaniu, w ramach Studiów II stopnia na Kierunku Informatyka.

Materiał powinien być wykorzystywany w połączeniu z dodatkowymi wyjaśnieniami, na przykład ze strony Prowadzącego zajęcia.

Kod zawarty w repozytorium służy tylko i wyłącznie do celom edukacyjnych. W szczególności nie powinien być wykorzystywany produkcyjnie z uwagi na zawarte w nim uproszczenia.


### Źródła

Szczegółowe odwołania do źródeł, na bazie których powstawały załączone przykłady, znaleźć można na platformie Moodle oraz w materiałach prezentowanych w trakcie zajęć.

Poniżej wybrane źródła:
* https://spring.io/guides/gs/spring-boot/, VMware, Inc. or its affiliates.
* https://spring.io/guides/gs/serving-web-content/, VMware, Inc. or its affiliates.
* https://spring.io/guides/gs/accessing-data-mysql/, VMware, Inc. or its affiliates.
* https://spring.io/guides/gs/rest-service/, VMware, Inc. or its affiliates.
* https://spring.io/guides/tutorials/rest/, VMware, Inc. or its affiliates.
